echo off
cls

echo This will run VMAF calculation for all the files, in batches so it doesn't hammer your CPU too much.
echo VMAF doesn't need to run at realtime, hence running all at once is possible.
echo.
echo These are the raw-video based maxq results
echo Press enter to run the lossless-input_maxq batch
pause

.\ffmpeg.exe -i lossless-input_maxq/libx264_game1.mp4 -i source-files/lossless/game1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libx264_game1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libx264_lecturer1.mp4 -i source-files/lossless/lecturer1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libx264_lecturer1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libx265_game1.mp4 -i source-files/lossless/game1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libx265_game1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libx265_lecturer1.mp4 -i source-files/lossless/lecturer1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libx265_lecturer1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libvpx-vp9_game1.mp4 -i source-files/lossless/game1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libvpx-vp9_game1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libvpx-vp9_lecturer1.mp4 -i source-files/lossless/lecturer1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libvpx-vp9_lecturer1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libsvtav1_game1.mp4 -i source-files/lossless/game1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libsvtav1_game1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libsvtav1_lecturer1.mp4 -i source-files/lossless/lecturer1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libsvtav1_lecturer1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libaom-av1_game1.mp4 -i source-files/lossless/game1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libaom-av1_game1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossless-input_maxq/libaom-av1_lecturer1.mp4 -i source-files/lossless/lecturer1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_maxq/libaom-av1_lecturer1.json:log_fmt=json:n_threads=16" -f null -



cls
echo You should probably wait for all the other calculation to be done, unless your CPU usage is still low
echo These are the speed-based maxp results
echo Press enter to run the lossy-input_maxq batch
pause

.\ffmpeg.exe -i lossy-input_maxq/libx264_vid1.mp4 -i source-files/lossy/vid1.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libx264_vid1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libx264_vid2.mp4 -i source-files/lossy/vid2.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libx264_vid2.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libx264_vid3.mp4 -i source-files/lossy/vid3.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libx264_vid3.json:log_fmt=json:n_threads=16" -f null -

.\ffmpeg.exe -i lossy-input_maxq/libx265_vid1.mp4 -i source-files/lossy/vid1.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libx265_vid1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libx265_vid2.mp4 -i source-files/lossy/vid2.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libx265_vid2.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libx265_vid3.mp4 -i source-files/lossy/vid3.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libx265_vid3.json:log_fmt=json:n_threads=16" -f null -

.\ffmpeg.exe -i lossy-input_maxq/libvpx-vp9_vid1.mp4 -i source-files/lossy/vid1.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libvpx-vp9_vid1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libvpx-vp9_vid2.mp4 -i source-files/lossy/vid2.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libvpx-vp9_vid2.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libvpx-vp9_vid3.mp4 -i source-files/lossy/vid3.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libvpx-vp9_vid3.json:log_fmt=json:n_threads=16" -f null -

.\ffmpeg.exe -i lossy-input_maxq/libsvtav1_vid1.mp4 -i source-files/lossy/vid1.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libsvtav1_vid1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libsvtav1_vid2.mp4 -i source-files/lossy/vid2.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libsvtav1_vid2.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libsvtav1_vid3.mp4 -i source-files/lossy/vid3.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libsvtav1_vid3.json:log_fmt=json:n_threads=16" -f null -

.\ffmpeg.exe -i lossy-input_maxq/libaom-av1_vid1.mp4 -i source-files/lossy/vid1.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libaom-av1_vid1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libaom-av1_vid2.mp4 -i source-files/lossy/vid2.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libaom-av1_vid2.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i lossy-input_maxq/libaom-av1_vid3.mp4 -i source-files/lossy/vid3.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossy-input_maxq/libaom-av1_vid3.json:log_fmt=json:n_threads=16" -f null -



cls
echo Same thing applies, wait or run straight away
echo These are the speed-based preset results for libx264
echo Press enter to run the speed-test_maxp batch
pause

.\ffmpeg.exe -i speed-test_maxp/libx264.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_maxp/libx264.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_maxp/libx265.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_maxp/libx265.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_maxp/libvpx-vp9.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_maxp/libvpx-vp9.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_maxp/libaom-av1.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_maxp/libaom-av1.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_maxp/libsvtav1.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_maxp/libsvtav1.json:log_fmt=json:n_threads=16" -f null -



cls
echo Same thing applies, wait or run straight away
echo These are the speed-based preset results for libx264
echo Press enter to run the speed-test_presets libx264 batch
pause

.\ffmpeg.exe -i speed-test_presets/preset-superfast_libx264.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-superfast_libx264.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-veryfast_libx264.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-veryfast_libx264.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-faster_libx264.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-faster_libx264.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-fast_libx264.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-fast_libx264.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-medium_libx264.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-medium_libx264.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-slow_libx264.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-slow_libx264.json:log_fmt=json:n_threads=16" -f null -



cls
echo Same thing applies, wait or run straight away
echo These are the speed-based preset results for libx265
echo Press enter to run the speed-test_presets libx265 batch
echo.
echo This is the last batch, so after this it's goodbye
pause

.\ffmpeg.exe -i speed-test_presets/preset-superfast_libx265.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-superfast_libx265.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-veryfast_libx265.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-veryfast_libx265.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-faster_libx265.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-faster_libx265.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-fast_libx265.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-fast_libx265.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-medium_libx265.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-medium_libx265.json:log_fmt=json:n_threads=16" -f null -
.\ffmpeg.exe -i speed-test_presets/preset-slow_libx265.mp4 -i source-files/speed-test/speed-test.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=speed-test_presets/preset-slow_libx265.json:log_fmt=json:n_threads=16" -f null -

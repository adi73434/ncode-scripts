# Speed Testing on an MP4 file
Please only run these one at a time; they are in a Markdown file for a reason.

# Fastest

### ffmpeg command fastest libx264:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx264 -preset ultrafast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M maxp_libx264_speed-test.mp4`
### ffmpeg command fastest libx265:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx265 -preset ultrafast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M maxp_libx265_speed-test.mp4`
### ffmpeg command fastest libvpx-vp9:
`.\ffmpeg -i source/speed-test.mp4 -c:v libvpx-vp9 -minrate 10M -maxrate 10M -b:v 10M -deadline realtime -cpu-used 8 -row-mt 1 -threads 16 maxp_libvpx-vp9_speed-test.mp4`
### ffmpeg command fastest libaom-av1:
`.\ffmpeg -i source/speed-test.mp4 -c:v libaom-av1 -b:v 10M -cpu-used 8 -row-mt 1 -threads 16 -tile-columns 4 -tile-rows 4 maxp_libaom-av1_speed-test.mp4`
### ffmpeg command fastest libsvtav1:
`.\ffmpeg -i source/speed-test.mp4 -c:v libsvtav1 -b:v 10M -preset 8 -row-mt 1 -threads 16 -tile-columns 4 -tile-rows 4 maxp_libsvtav1_speed-test.mp4`

<br>
<br>
<br>

# libx264 Presets
### ffmpeg command 'superfast' libx264:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx264 -preset superfast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-superfast_libx264_speed-test.mp4`
### ffmpeg command 'veryfast' libx264:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx264 -preset veryfast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-veryfast_libx264_speed-test.mp4`
### ffmpeg command 'faster' libx264:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx264 -preset faster -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-faster_libx264_speed-test.mp4`
### ffmpeg command 'fast' libx264:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx264 -preset fast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-fast_libx264_speed-test.mp4`
### ffmpeg command 'medium' libx264:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx264 -preset medium -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-medium_libx264_speed-test.mp4`
### ffmpeg command 'slow' libx264:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx264 -preset slow -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-slow_libx264_speed-test.mp4`

<br>
<br>
<br>

# libx265 Presets
### ffmpeg command 'superfast' libx265:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx265 -preset superfast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-superfast_libx265_speed-test.mp4`
### ffmpeg command 'veryfast' libx265:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx265 -preset veryfast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-veryfast_libx265_speed-test.mp4`
### ffmpeg command 'faster' libx265:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx265 -preset faster -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-faster_libx265_speed-test.mp4`
### ffmpeg command 'fast' libx265:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx265 -preset fast -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-fast_libx265_speed-test.mp4`
### ffmpeg command 'medium' libx265:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx265 -preset medium -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-medium_libx265_speed-test.mp4`
### ffmpeg command 'slow' libx265:
`.\ffmpeg -i source/speed-test.mp4 -c:v libx265 -preset slow -b:v 10M -minrate 10M -maxrate 10M -bufsize 20M  preset-slow_libx265_speed-test.mp4`
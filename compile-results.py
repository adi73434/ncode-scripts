import json
from glob import glob

# See: https://stackoverflow.com/a/16892491
def remove_prefix(text, prefix):
	return text[text.startswith(prefix) and len(prefix):]



# ------------------------------------------------------------------------------
# Paths of results
# ------------------------------------------------------------------------------
paths = [
	"lossless-input_maxq",
	"lossless-input_ncode-app-footage",
	"lossy-input_maxq",
	"ncode-stream-triangulation",
	"ncode-vod-triangulation",
	"speed-test_maxp",
	"speed-test_presets"
]



# ------------------------------------------------------------------------------
# Gather all JSON data
# ------------------------------------------------------------------------------
data = {}
for file_path in paths:
	for file_name in glob(file_path + "/*.json"):
		with open(file_name) as f:
			# Don't need the escaped backslashes nor the file extension
			# as these will be read by "humans", or more likely Excel and then humans
			new_name = file_name.replace("\\", "____")
			new_name = new_name.replace(".json", "")
			data[new_name] = json.load(f)



# ------------------------------------------------------------------------------
# I chose to get all VMAF results, but you can swap the comments around if you only care about means
# ------------------------------------------------------------------------------
# harmonic_mean_results = {}
# mean_results = {}
vmaf_results = {}

for stored_data in data:
	# harmonic_mean_results[stored_data] = data[stored_data]["pooled_metrics"]["vmaf"]["harmonic_mean"]
	# mean_results[stored_data] = data[stored_data]["pooled_metrics"]["vmaf"]["mean"]
	vmaf_results[stored_data] = data[stored_data]["pooled_metrics"]["vmaf"]

vmaf_per_frame_results = {}

for stored_data in data:
	# This adds an object to the object; it needs to exist so that the frame data can be added to it
	vmaf_per_frame_results[stored_data] = {}
	for frame in data[stored_data]["frames"]:
		# Add the VMAF scores directly under the stored_data index, with +1 iteration per frame
		vmaf_per_frame_results[stored_data][frame["frameNum"]] = frame["metrics"]["vmaf"]



# ------------------------------------------------------------------------------
# If you want these, uncomment matching as above
# ------------------------------------------------------------------------------
# f_hmr = open("./compile-results/vmaf_harmonic-mean_all.json", "w")
# f_hmr.write(json.dumps(harmonic_mean_results))
# f_hmr.close()
# f_mr = open("./compile-results/vmaf_mean_all.json", "w")
# f_mr.write(json.dumps(mean_results))
# f_mr.close()



# ------------------------------------------------------------------------------
# All VMAF
# ------------------------------------------------------------------------------
f_w = open("./compile-results/vmaf_all.json", "w")
f_w.write(json.dumps(vmaf_results))
f_w.close()



# ------------------------------------------------------------------------------
# All VMAF per frame
# ------------------------------------------------------------------------------
f_w = open("./compile-results/vmaf_per-frame_all.json", "w")
f_w.write(json.dumps(vmaf_per_frame_results))
f_w.close()



# ------------------------------------------------------------------------------
# Specific VMAF per frame
# ------------------------------------------------------------------------------
for compiled_data in vmaf_per_frame_results:
	f_w = open("./compile-results/vmaf_per-frame_per-file/" + str(compiled_data) + ".json", "w")
	f_w.write(json.dumps(vmaf_per_frame_results[compiled_data]))
	f_w.close()
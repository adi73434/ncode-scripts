echo off
cls

echo Same thing applies as the _vmaf-all.bat file
echo This is checking the quality of the configured
echo encoder parameters for all possible options in the ncode Test App.
echo This is doing the VODs first.
echo.
pause

start cmd /c .\ffmpeg.exe -i ncode-vod-triangulation/a_libx264.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-vod-triangulation/a_libx264.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-vod-triangulation/b_libx264.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-vod-triangulation/b_libx264.json:log_fmt=json:n_threads=4" -f null -

start cmd /c .\ffmpeg.exe -i ncode-vod-triangulation/a_libx265.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-vod-triangulation/a_libx265.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-vod-triangulation/b_libx265.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-vod-triangulation/b_libx265.json:log_fmt=json:n_threads=4" -f null -

start cmd /c .\ffmpeg.exe -i ncode-vod-triangulation/a_libvpx-vp9.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-vod-triangulation/a_libvpx-vp9.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-vod-triangulation/b_libvpx-vp9.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-vod-triangulation/b_libvpx-vp9.json:log_fmt=json:n_threads=4" -f null -



cls
echo You should probably wait
echo This next batch is for libx264 game streams
echo.
pause

start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx264_qual1.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx264_qual1.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx264_qual2.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx264_qual2.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx264_qual3.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx264_qual3.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx264_qual4.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx264_qual4.json:log_fmt=json:n_threads=4" -f null -



cls
echo Again.
echo This time for libx264 lecturer streams
echo.
pause

start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx264_qual1.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx264_qual1.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx264_qual2.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx264_qual2.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx264_qual3.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx264_qual3.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx264_qual4.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx264_qual4.json:log_fmt=json:n_threads=4" -f null -



cls
echo Again.
echo This time for libx265 game streams
echo.
pause

start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx265_qual1.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx265_qual1.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx265_qual2.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx265_qual2.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx265_qual3.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx265_qual3.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libx265_qual4.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libx265_qual4.json:log_fmt=json:n_threads=4" -f null -



cls
echo Again.
echo This time for libx265 lecturer streams
echo.
pause
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx265_qual1.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx265_qual1.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx265_qual2.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx265_qual2.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx265_qual3.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx265_qual3.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libx265_qual4.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libx265_qual4.json:log_fmt=json:n_threads=4" -f null -



cls
echo Again.
echo This time for libvpx-vp9 game streams
echo.
pause
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libvpx-vp9_qual1.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libvpx-vp9_qual1.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libvpx-vp9_qual2.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libvpx-vp9_qual2.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libvpx-vp9_qual3.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libvpx-vp9_qual3.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/a_libvpx-vp9_qual4.mp4 -i source-files/ncode-footage/a.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/a_libvpx-vp9_qual4.json:log_fmt=json:n_threads=4" -f null -



cls
echo Again.
echo This time for libvpx-vp9 lecturer streams
echo.
pause

start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libvpx-vp9_qual1.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libvpx-vp9_qual1.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libvpx-vp9_qual2.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libvpx-vp9_qual2.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libvpx-vp9_qual3.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libvpx-vp9_qual3.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i ncode-stream-triangulation/b_libvpx-vp9_qual4.mp4 -i source-files/ncode-footage/b.mp4 -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=ncode-stream-triangulation/b_libvpx-vp9_qual4.json:log_fmt=json:n_threads=4" -f null -

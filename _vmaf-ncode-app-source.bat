echo off
cls

echo Same thing applies as the _vmaf-all.bat file
echo This was the attempt at transcoding AVI
echo files to visually-lossless libx264 for playback in the Test App.
echo However, these had issues with playback in the Test App so they're unused.


start cmd /c .\ffmpeg.exe -i lossless-input_ncode-app-footage/a.mp4 -i source-files/lossless/game1.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_ncode-app-footage/a.json:log_fmt=json:n_threads=4" -f null -
start cmd /c .\ffmpeg.exe -i lossless-input_ncode-app-footage/b.mp4 -i source-files/lossless/lecturer2.avi -lavfi libvmaf="model_path=model/vmaf_v0.6.1.json:log_path=lossless-input_ncode-app-footage/b.json:log_fmt=json:n_threads=4" -f null -

echo off
cls

echo This will encode the source footage used in the Test App
echo This will use the same presets available within the codebase for video-on-demand
echo.
pause

REM ----------------------------------------------------------------------------
REM using visually lossless libx264 input files which were used in ncode
REM ----------------------------------------------------------------------------


.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx264 -preset veryfast -crf 20 ncode-vod-triangulation/a_libx264.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx264 -preset veryfast -crf 20 ncode-vod-triangulation/b_libx264.mp4


.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx265 -preset veryfast -crf 20 ncode-vod-triangulation/a_libx265.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx265 -preset veryfast -crf 20 ncode-vod-triangulation/b_libx265.mp4


.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libvpx-vp9 -b:v 0 -crf 60 -cpu-used 8 -deadline realtime -row-mt 1 ncode-vod-triangulation/a_libvpx-vp9.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libvpx-vp9 -b:v 0 -crf 60 -cpu-used 8 -deadline realtime -row-mt 1 ncode-vod-triangulation/b_libvpx-vp9.mp4



cls
echo This will encode the source footage used in the Test App
echo This will use the same presets available within the codebase for video-stream
echo Note that these are using the default 750000Byte/s
echo.
pause

.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx264 -preset superfast -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx264_qual1.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx264 -preset faster -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx264_qual2.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx264 -preset medium -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx264_qual3.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx264 -preset slow -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx264_qual4.mp4

.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx264 -preset superfast -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx264_qual1.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx264 -preset faster -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx264_qual2.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx264 -preset medium -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx264_qual3.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx264 -preset slow -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx264_qual4.mp4


.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx265 -preset ultrafast -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx265_qual1.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx265 -preset superfast -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx265_qual2.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx265 -preset veryfast -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx265_qual3.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libx265 -preset faster -crf 20 -b:v 7.5M ncode-stream-triangulation/a_libx265_qual4.mp4

.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx265 -preset ultrafast -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx265_qual1.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx265 -preset superfast -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx265_qual2.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx265 -preset veryfast -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx265_qual3.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libx265 -preset faster -crf 20 -b:v 7.5M ncode-stream-triangulation/b_libx265_qual4.mp4


.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libvpx-vp9 -cpu-used 8 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/a_libvpx-vp9_qual1.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libvpx-vp9 -cpu-used 7 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/a_libvpx-vp9_qual2.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libvpx-vp9 -cpu-used 6 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/a_libvpx-vp9_qual3.mp4
.\ffmpeg -i source-files/ncode-footage/a.mp4 -c:v libvpx-vp9 -cpu-used 5 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/a_libvpx-vp9_qual4.mp4

.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libvpx-vp9 -cpu-used 8 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/b_libvpx-vp9_qual1.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libvpx-vp9 -cpu-used 7 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/b_libvpx-vp9_qual2.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libvpx-vp9 -cpu-used 6 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/b_libvpx-vp9_qual3.mp4
.\ffmpeg -i source-files/ncode-footage/b.mp4 -c:v libvpx-vp9 -cpu-used 5 -deadline realtime -row-mt 1 -b:v 7.5M ncode-stream-triangulation/b_libvpx-vp9_qual4.mp4

echo off
cls

echo This will just be transcoding the lossless utvideo AVI videos
echo into a visually lossless libx264 file.
echo This is so that decoding is faster, because decoding AVI is horrendously slow.
echo.
pause

REM ----------------------------------------------------------------------------
REM visually lossless libx264
REM ----------------------------------------------------------------------------

.\ffmpeg -i source-files/lossless/game1.avi -c:v libx264 -preset placebo -crf 16 lossless-input_ncode-app-footage/a.mp4
.\ffmpeg -i source-files/lossless/lecturer2.avi -c:v libx264 -preset placebo -crf 16 lossless-input_ncode-app-footage/b.mp4

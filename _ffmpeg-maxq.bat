echo off
cls

echo These are the quality-based encoding commands
echo These are not being measured or observed for speed or performance
echo Hence, you may run these all at once... if you so desire, 
echo but your CPU probably won't thank you for that, and you may find that it'll take longer.
echo.
pause

REM ----------------------------------------------------------------------------
REM lossless utvideo AVI input files
REM ----------------------------------------------------------------------------

.\ffmpeg -i source-files/lossless/game1.avi -c:v libx264 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossless-input_maxq/libx264_game1.mp4
.\ffmpeg -i source-files/lossless/lecturer1.avi -c:v libx264 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossless-input_maxq/libx264_lecturer1.mp4

.\ffmpeg -i source-files/lossless/game1.avi -c:v libx265 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossless-input_maxq/libx265_game1.mp4
.\ffmpeg -i source-files/lossless/lecturer1.avi -c:v libx265 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossless-input_maxq/libx265_lecturer1.mp4

.\ffmpeg -i source-files/lossless/game1.avi -c:v libvpx-vp9 -minrate 2M -maxrate 2M -b:v 2M -deadline best -cpu-used 0 -row-mt 1 -threads 16 lossless-input_maxq/libvpx-vp9_game1.mp4
.\ffmpeg -i source-files/lossless/lecturer1.avi -c:v libvpx-vp9 -minrate 2M -maxrate 2M -b:v 2M -deadline best -cpu-used 0 -row-mt 1 -threads 16 lossless-input_maxq/libvpx-vp9_lecturer1.mp4

.\ffmpeg -i source-files/lossless/game1.avi -c:v libsvtav1 -b:v 2M -preset 0 -row-mt 1 -threads 12 lossless-input_maxq/libsvtav1_game1.mp4
.\ffmpeg -i source-files/lossless/lecturer1.avi -c:v libsvtav1 -b:v 2M -preset 0 -row-mt 1 -threads 12 lossless-input_maxq/libsvtav1_lecturer1.mp4

.\ffmpeg -i source-files/lossless/game1.avi -c:v libaom-av1 -b:v 2M -cpu-used 0 -row-mt 1 -threads 12 lossless-input_maxq/libaom-av1_game1.mp4
.\ffmpeg -i source-files/lossless/lecturer1.avi -c:v libaom-av1 -b:v 2M -cpu-used 0 -row-mt 1 -threads 12 lossless-input_maxq/libaom-av1_lecturer1.mp4


REM ----------------------------------------------------------------------------
REM CRF 20 x264 input files
REM ----------------------------------------------------------------------------

.\ffmpeg -i source-files/lossy/vid1.mp4 -c:v libx264 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossy-input_maxq/libx264_vid1.mp4
.\ffmpeg -i source-files/lossy/vid1.mp4 -c:v libx265 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossy-input_maxq/libx265_vid1.mp4
.\ffmpeg -i source-files/lossy/vid1.mp4 -c:v libvpx-vp9 -minrate 2M -maxrate 2M -b:v 2M -deadline best -cpu-used 0 -row-mt 1 -threads 16 lossy-input_maxq/libvpx-vp9_vid1.mp4
.\ffmpeg -i source-files/lossy/vid1.mp4 -c:v libsvtav1 -b:v 2M -preset 0 -row-mt 1 -threads 12 lossy-input_maxq/libsvtav1_vid1.mp4
.\ffmpeg -i source-files/lossy/vid1.mp4 -c:v libaom-av1 -b:v 2M -cpu-used 0 -row-mt 1 -threads 12 lossy-input_maxq/libaom-av1_vid1.mp4


.\ffmpeg -i source-files/lossy/vid2.mp4 -c:v libx264 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossy-input_maxq/libx264_vid2.mp4
.\ffmpeg -i source-files/lossy/vid2.mp4 -c:v libx265 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossy-input_maxq/libx265_vid2.mp4
.\ffmpeg -i source-files/lossy/vid2.mp4 -c:v libvpx-vp9 -minrate 2M -maxrate 2M -b:v 2M -deadline best -cpu-used 0 -row-mt 1 -threads 16 lossy-input_maxq/libvpx-vp9_vid2.mp4
.\ffmpeg -i source-files/lossy/vid2.mp4 -c:v libsvtav1 -b:v 2M -preset 0 -row-mt 1 -threads 12 lossy-input_maxq/libsvtav1_vid2.mp4
.\ffmpeg -i source-files/lossy/vid2.mp4 -c:v libaom-av1 -b:v 2M -cpu-used 0 -row-mt 1 -threads 12 lossy-input_maxq/libaom-av1_vid2.mp4


.\ffmpeg -i source-files/lossy/vid3.mp4 -c:v libx264 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossy-input_maxq/libx264_vid3.mp4
.\ffmpeg -i source-files/lossy/vid3.mp4 -c:v libx265 -preset placebo -b:v 2M -minrate 2M -maxrate 2M -bufsize 4M lossy-input_maxq/libx265_vid3.mp4
.\ffmpeg -i source-files/lossy/vid3.mp4 -c:v libvpx-vp9 -minrate 2M -maxrate 2M -b:v 2M -deadline best -cpu-used 0 -row-mt 1 -threads 16 lossy-input_maxq/libvpx-vp9_vid3.mp4
.\ffmpeg -i source-files/lossy/vid3.mp4 -c:v libsvtav1 -b:v 2M -preset 0 -row-mt 1 -threads 12 lossy-input_maxq/libsvtav1_vid3.mp4
.\ffmpeg -i source-files/lossy/vid3.mp4 -c:v libaom-av1 -b:v 2M -cpu-used 0 -row-mt 1 -threads 12 lossy-input_maxq/libaom-av1_vid3.mp4


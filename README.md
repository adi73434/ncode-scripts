# 1 ncode-scripts
Links:
- [Google Drive zip with everything](https://drive.google.com/drive/folders/1v2YHE4Gnd93GH2GI5Z0vV5t-jjYE7APD?usp=sharing)
- [This GitLab repository](https://gitlab.com/adi73434/ncode-scripts)
- [Test App GitLab repository](https://gitlab.com/adi73434/ncode)


This is the supporting repository for my dissertation and the ncode Test App.

For the fully populated version, with all of the video files, see the Google Drive zip.



<br><br>



# 2 Sources
If you use your own footage and re-create the folders, or download the full archive from the Google Drive link in the top of the file, this is more or less the way your folder should look (potentially excluding the `.git` and/or `.history` folders):

![folder-look](./readme-assets/folder-look.png)


## 2.1 ffmpeg
If you want to use this, you should download ffmpeg and put the following files into the root directory:
- `ffmpeg.exe`
- `ffprobe.exe`
- `ffplay.exe`

I downloaded the ffmpeg build used herein from https://www.gyan.dev/ffmpeg/builds/

The current version used is as follows, which can be checked by running `.\ffmpeg.exe -copyright`

>ffmpeg version 4.4-full_build-www.gyan.dev Copyright (c) 2000-2021 the FFmpeg developers

Also note that you don't actually need `ffprobe.exe` or `ffplay.exe` for what's being used in these scripts.



## 2.2 Netflix VMAF
The VMAF tests utilise ffmpeg.

The `./model/` folder is sourced from [Netflix's VMAF repository](https://github.com/Netflix/vmaf).



## 2.3 Videos
The `./source-files/` folder currently has the following subfolders, which are not included in the GitLab repository:
- `lossless`
- `lossy`
- `ncode-footage`
- `speed-test`

The output folders will also not be populated in the GitLab repository with the `mp4` or `avi` files, because storing those on GitLab isn't a great idea.



<br><br>



# 3 Usage
You can run any of the `_ffmpeg` bat files to _create_ the output videos.

Following which, the `_vmaf-all.bat` file will compute the VMAF-and-other scores for most of the files, with the two additional files being `_vmaf-ncode-app-source.bat` and `_vmaf-ncode-triangulation.bat`.

Please note, however, that the vmaf batch files assume all the files exist because I hard-coded them, and they would ideally have been done with for loops.



## 3.1 [compile-results.py](./compile-results.py)
Once you have everything computed, you can run `compile-results.py` to "compile" all the results into one file. Currently, this gets all the VMAF scores, outputting them into `./compile-results/vmaf_all.json`.

It also gets VMAF-per-frame results for all files, outputting them to `./compile-rsults/vmaf_per-frame_all.json`, and for any individual specified files, it outputs them to the `./compile-results/vmaf_per-frame_per-file/` folder with the encoder/name as the filename.

Please note that this file is likely going to be "beautified" via a VSCode extension, which is why it won't look minified. This is just because I was checking the data.



## 3.2 Speed test 
For the speed test results, please consult [_ffmpeg-speed-test.md](_ffmpeg-speed-test.md) or [_ffmpeg-speed-test.html](_ffmpeg-speed-test.html) as those are not being automated and you should ensure that you have a "sterile" Windows environment and PC.

By this, I mean:
- very little running apps;
- LTSC/LTSB version of Windows, or a stripped version of Windows 10 Pro or whatever you have;
- a fixed CPU clock speed, or a PState clock setting instead of Intel's Turbo Boost or AMD's Precision Boost - those will induce clock variance which may (slightly) impact speed results;
- and no possibility of thermal throttling, among other potential things.



<br><br>



# 4 Encoding



## 4.1 [_ffmpeg-maxq.bat](_ffmpeg-maxq.bat)
Outputs:
- `./lossless-input_maxq/`
- `./lossy-input_maxq/`

This will encode the input files with the highest quality settings for the specified encoders.

Note that this will take a long while, so these serve essentially as a hypothetical maximum quality-per-bitrate for each encoder (not considering additional flags, though).



## 4.2 [_ffmpeg-ncode-app-source.bat](./_ffmpeg-ncode-app-source.bat)
Inputs:
- `./source-files/lossless/`

Outputs:
- `./lossless-input_ncode-app-footage/`

This was used to try to encode the lossless source files to use them in the Test App, but they were not playable within the video-stream sections, so different files were used in the Test App.



## 4.3 [_ffmpeg-ncode-triangulation.bat](./_ffmpeg-ncode-triangulation.bat)
Inputs:
- `./source-files/ncode-footage/`

Outputs:
- `./ncode-vod-triangulation/`
- `./ncode-stream-triangulation/`

This encodes the actual footage used in the Test App using the ffmpeg CLI parameters which are analogous to the parameters used within the C++ code base.

As such, the VODs produce one quality setting, whereas the streams are suffixed with `_qualN`, where N is the quality available in the UI.



## 4.4 [_ffmpeg-speed-test.md](./_ffmpeg-speed-test.md)
As mentioned before, this file is a slightly separate test and I made it a little annoying on purpose because it's supposed to be done while observing the actual command prompt.

Also, this gives you explicit breaks where you should be observing Task Manager, though preferably also use [HWiNFO64](https://www.hwinfo.com/download/) or [MSI Afterburner](https://www.msi.com/Landing/afterburner/graphics-cards) for more accurate CPU usage reporting.



## 4.5 Additional things
The max_q tests have already been ran, but for an unofficial speedtest for libsvtav1 just to prove that it is not as slow as "thousands of times slower than realtime", I ran this. CMD commant

```batch
powershell -command "(New-TimeSpan -Start (Get-Date "01/01/1970") -End (Get-Date)).TotalSeconds" > EPOCH_START.txt && ^
.\ffmpeg -i source-files/lossless/game1.avi -c:v libsvtav1 -b:v 2M -preset 0 -row-mt 1 -threads 16 test.mp4 && ^
powershell -command "(New-TimeSpan -Start (Get-Date "01/01/1970") -End (Get-Date)).TotalSeconds" > EPOCH_END.txt
```



<br><br>



# 5 VMAF Analysis
These are the quality analysis runs for the encoded files.

Also, since the run time of the VMAF analysis does not at all impact the results, how you run these doesn't really matter.



## 5.1 Run in Batches
These can either be ran "multiple at once, in batches", with a lower `n_threads` value for each active VMAF instance.

To do this, add `start cmd /c ` as a prefix to every `.\ffmpeg.exe`. To do this, I recommend using VSCode and then selecting the `.\ffmpeg.exe` text, holding `Ctrl` + `D`, then typing or pasting the `start cmd /c`, which will change them all at once.

If you want to run it like this, you should **uncomment** the `pause` statements.


## 5.2 In Series with more threads
You can also run the VMAF analysis one by one. It doesn't really matter how you do it.

To do this, **comment** out the `pause` statements, removfe the `start cmd/c` prefix from all the commands, and modify the `n_threads` value to your liking. I've a 8C/16T procesor, so I use 16T.



## 5.3 [_vmaf-all.bat](./_vmaf-all.bat)
Inputs:
- `./lossless-input_maxq/`
- `./lossy-input_maxq/`
- `./speed-test_maxp/`
- `./speed-test_presets/`

_Please note that this takes the encoded output and the source-files as input, so see the encoders for which source-files they actually use._

This runs the VMAF analysis for _most_ of the output files - yes, I realise the name is misleading.



## 5.4 [_vmaf-ncode-app-source.bat](./_vmaf-ncode-app-source.bat)
Inputs:
- `./lossless-input_ncode-app-footage/`

This was the file set up to run the VMAF analysis of the footage that was _intended_ to be used in the Test App. This was to ensure that the output footage was near enough lossless.

The reason I chose to do this was because AVI files are slow to decode, so using visually-lossless files instead of lossless and really hard to decode files would probably be better as the slow decoding would impact overall time/performance.

Nonetheless, different footage was used, so you can generally just ignore this.



## 5.5 [_vmaf-ncode-vod-triangulation.bat](./_vmaf-ncode-triangulation.bat)
Inputs:
- `./ncode-vod-triangulation/`
- `./ncode-stream-triangulation/`

This checks the quality of the ffmpeg CLI encoded footage, which aims to replicate the Test App encoding results.

These results should match the VOD footage produced by the Test App directly, except for any possible mishaps or differences in implementation with the libraries.

However, as for the stream encoded videos, these obviously do not have the issue of not playing back in realtime, as they technically have unlimited time to encode. Therefore, they are merely a baseline for what is expected for participants to say regarding the _visual quality_ of the streams, but any potential lag caused by below-1xspeed performance firnwill impact the results within the Test App.
